<?php

namespace ibraheem_91\AttachmentManager\Helpers;

use DateTime;
use DateTimeZone;

/**
 * Class Common
 *
 * @package ibraheem_91\AttachmentManager\Helpers
 */
class Common
{
    /**
     * Checks if the provided data is a valid JSON
     *
     * @param $string
     *
     * @return bool
     */
    public static function isValidJson($string)
    {
        $decoded = json_decode($string);

        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * Formats date in the given `DateTime` format
     *
     * @param        $dateTime
     * @param string $format
     * @param string $timezone The timezone in which the date is
     *
     * @return string
     */
    public static function formatDate($dateTime, $format = DateTime::ISO8601, $timezone = 'UTC')
    {
        $dateTime = new DateTime($dateTime, new DateTimeZone($timezone));

        return $dateTime->format($format);
    }
}

<?php

namespace ibraheem_91\AttachmentManager\Exceptions;

/**
 * Class ResponseException
 *
 * @package ibraheem_91\AttachmentManager\Exceptions
 */
class ResponseException extends HttpException
{
    /** @var string */
    public $status = '400';
    /** @var string */
    public $title = 'Bad Request Exception';
    /** @var string */
    public $detail = '';

    /**
     * BadRequestException constructor.
     *
     * @param string $detail
     * @param        $code
     * @param string $title
     */
    public function __construct($detail, $code, $title = '')
    {
        $this->detail = $detail ?: $this->detail;
        $this->title  = $title ?: $this->title;
        $this->status = $code;
        parent::__construct($this->detail);
    }
}
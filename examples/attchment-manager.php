<?php

require __DIR__ . '/../vendor/autoload.php';

use ibraheem_91\AttachmentManager\Clients\AttachmentManager;
use ibraheem_91\AttachmentManager\GuzzleHandler;

$baseUrl = 'http://xyz.ibra-node.com';


// Create a handler object and pass it to the client.
// It will be used for making API calls. Used the adapter
// pattern here to decouple the API calls and in case
// we need to replace guzzle with something else, we can
// do that easily
$handler           = new GuzzleHandler($baseUrl);
$attachmentManager = new AttachmentManager($handler);

$result = $attachmentManager->connectTokenCallbackUrl('zxde');
var_export($result);
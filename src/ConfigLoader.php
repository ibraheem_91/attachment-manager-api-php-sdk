<?php

namespace ibraheem_91\AttachmentManager;

use __;
use ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException;

/**
 * Class ConfigLoader
 *
 * @package ibraheem_91\AttachmentManager
 */
class ConfigLoader
{
    /**
     * Returns the endpoint detail i.e. array consisting of 'method' and 'endpoint'
     *
     * @param       $configString
     * @param array $pathVariables
     *
     * @return array
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     */
    public static function getEndpoint($configString, $pathVariables = [])
    {
        $rawEndpoint = static::get($configString, false);

        if ($rawEndpoint === false) {
            throw new InvalidEndpointException(sprintf("Unable to get the endpoint '%s'", $configString));
        }
        
        $endpointDetail = explode(' ', $rawEndpoint);

        $method = $endpointDetail[0];
        $path   = $endpointDetail[1];
        if (!empty($pathVariables)) {
            foreach ($pathVariables as $varName => $varValue) {
                $path = str_replace('{' . $varName . '}', $varValue, $path);
            }
        }

        return compact('method', 'path');
    }

    /**
     * Obtains the value from the configuration file for the passed index
     *
     * @param        $identifier
     * @param string $defaultValue
     *
     * @return mixed
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidArgumentException
     */
    public static function get($identifier, $defaultValue = '')
    {
        // Remove the filename from the identifier
        $parts    = explode('.', $identifier);
        $fileData = static::load($parts[0]);
        unset($parts[0]);
        $identifier = implode('.', $parts);

        return __::get($fileData, $identifier, $defaultValue);
    }

    /**
     * Loads the data from passed file name considering that it is a .php file
     * and available under the `Config` folder
     *
     * @param $fileName
     *
     * @return array|mixed
     */
    public static function load($fileName)
    {
        $filePath = __DIR__ . '/Config/' . $fileName . '.php';

        if (is_file($filePath)) {
            return include $filePath;
        }

        return [];
    }
}

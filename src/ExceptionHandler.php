<?php

namespace ibraheem_91\AttachmentManager;

use __;
use ibraheem_91\AttachmentManager\Exceptions\InternalException;
use ibraheem_91\AttachmentManager\Exceptions\ResponseException;
use ibraheem_91\AttachmentManager\Helpers\Common;

/**
 * Class ExceptionHandler
 *
 * @package ibraheem_91\AttachmentManager
 */
class ExceptionHandler
{
    protected static $statusCodes = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
    ];

    /**
     * DESC
     *
     * @param $code
     * @param $response
     *
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InternalException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\ResponseException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public static function handleByCode($code, $response)
    {
        $errorDetail = static::getErrorDetail($response);
        $httpCodes   = array_keys(static::$statusCodes);
        if (in_array($code, $httpCodes)) {
            throw new ResponseException(
                static::getErrorTitle($response),
                $code,
                static::getErrorDetail($response)
            );
        } else {
            throw new InternalException($errorDetail);
        }
    }

    /**
     * Gets the error detail from the ApiException if the API problem
     * otherwise returns the whole response for the error
     *
     * @param $response
     *
     * @return mixed
     */
    public static function getErrorDetail($response)
    {
        if (static::isApiProblem($response)) {
            $decoded = json_decode($response, true);

            return $decoded['detail'];
        }

        return $response;
    }

    /**
     * Gets the error title from the ApiException if the API problem
     * otherwise returns the whole response for the error
     *
     * @param $response
     *
     * @return mixed
     */
    public static function getErrorTitle($response)
    {
        if (static::isApiProblem($response)) {
            $decoded = json_decode($response, true);

            return $decoded['title'];
        }

        return $response;
    }

    /**
     * Checks whether the exception thrown is a valid API problem.
     *
     * @param $string
     *
     * @return bool
     */
    public static function isApiProblem($string)
    {
        if (!Common::isValidJson($string)) {
            return false;
        }
        $decoded = json_decode($string, true);
        $status  = __::get($decoded, 'status', false);
        $title   = __::get($decoded, 'title', false);
        $detail  = __::get($decoded, 'detail', false);
        $type    = __::get($decoded, 'type', false);

        // Because the details that API sends has all of these
        return $status !== false &&
               $title !== false &&
               $detail !== false &&
               $type !== false;
    }
}


<?php
namespace ibraheem_91\AttachmentManager\Exceptions;

use Exception;

/**
 * Class HttpException
 *
 * @package ibraheem_91\AttachmentManager\Exceptions
 */
abstract class HttpException extends Exception
{
    /**
     * @var string
     */
    protected $id;
    /**
     * @var string
     */
    protected $status;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $detail;

    /**
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }

    /**
     * Get the status
     *
     * @return int
     */
    public function getStatus()
    {
        return (int)$this->status;
    }

    /**
     * Return the Exception as an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'status' => $this->status,
            'title'  => $this->title,
            'detail' => $this->detail,
            'type'   => !empty($this->type) ? $this->type : "https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
        ];
    }
}
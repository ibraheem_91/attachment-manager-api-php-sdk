<?php

/*
|--------------------------------------------------------------------------
| Endpoints Configuration
|--------------------------------------------------------------------------
|
| This file contains the configuration related to the endpoints. Keys represent
| the resources and values are the arrays of endpoints with keys representing
| how they will be accessed by the API provider and value as the endpoint detail
| i.e. method and endpoint
|
*/
return [
    'account' => [
        'create_account'             => 'POST /account',
        'create_mailbox'             => 'POST /account/mailbox',
        'connect_token_callback_url' => 'GET /account/connectTokenCallbackURL?contextio_token={contextio_token}',
        'get_active_mailboxes'       => 'GET /account/active-mailboxes/{primaryAccountId}',
        'sync_active_mailboxes'      => 'GET /account/sync-active-mailboxes/{primaryAccountId}',
        'retrieve_attachments'       => 'POST /account/attachments/{primaryAccountId}',
        'download_attachment'        => 'GET /account/attachments/{primaryAccountId}/files/{file_id}',
    ],
    'utility' => [
        'retrieve_mime_types' => 'GET /utility/mimetypes',
    ],
];

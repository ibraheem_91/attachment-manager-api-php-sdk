<?php

namespace ibraheem_91\AttachmentManager\Exceptions;

use Exception;

/**
 * Class InvalidEndpointException
 *
 * @package ibraheem_91\AttachmentManager\Exceptions
 */
class InvalidEndpointException extends Exception
{
}

<?php

namespace ibraheem_91\AttachmentManager\Contracts;

/**
 * Interface HandlerInterface
 *
 * @package ibraheem_91\AttachmentManager\Contracts
 * @author  Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
 *
 */
interface HandlerInterface
{
    /**
     * Sends GET request to a URL. Is a wrapper around `request` method
     *
     * @param string $endpoint
     * @param array  $options
     *
     * @return
     */
    public function get($endpoint, $options = []);

    /**
     * Sends POST request to a URL. Is a wrapper around `request` method
     *
     *
     * @param string $endpoint
     * @param array  $options
     *
     * @return mixed
     */
    public function post($endpoint, $options = []);

    /**
     * To send a request of any kind to the provided URL. Al the other methods
     * use this function to send HTTP reqeusts
     *
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $options
     *
     * @return mixed
     */
    public function request($method, $endpoint, $options = []);


    /**
     * Sends a PUT request to provided URL. Wraps around the request method
     *
     * @param       $endpoint
     * @param array $options
     *
     * @return mixed
     */
    public function put($endpoint, $options = []);

    /**
     * Sends a DELETE request to provided URL. Wraps around the request method
     *
     * @param string $endpoint
     * @param array  $options
     *
     * @return mixed
     */
    public function delete($endpoint, $options = []);
}

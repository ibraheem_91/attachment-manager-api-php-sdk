<?php

namespace ibraheem_91\AttachmentManager\Clients;

use ibraheem_91\AttachmentManager\ConfigLoader;
use ibraheem_91\AttachmentManager\Contracts\HandlerInterface;

/**
 * Class AttachmentManager
 *
 * @package ibraheem_91\AttachmentManager\Clients
 */
class AttachmentManager extends BaseClient
{
    //account
    const CREATE_ACCOUNT             = 'endpoints.account.create_account';
    const CREATE_MAILBOX             = 'endpoints.account.create_mailbox';
    const CONNECT_TOKEN_CALLBACK_URL = 'endpoints.account.connect_token_callback_url';
    const GET_ACTIVE_MAILBOXES       = 'endpoints.account.get_active_mailboxes';
    const SYNC_ACTIVE_MAILBOXES      = 'endpoints.account.sync_active_mailboxes';
    const RETRIEVE_ATTACHMENTS       = 'endpoints.account.retrieve_attachments';
    const DOWNLOAD_ATTACHMENT        = 'endpoints.account.download_attachment';

    //utility
    const RETRIEVE_MIME_TYPES = 'endpoints.utility.retrieve_mime_types';


    /**
     * AttachmentManager constructor.
     *
     * @param \ibraheem_91\AttachmentManager\Contracts\HandlerInterface $handler
     */
    public function __construct(HandlerInterface $handler)
    {
        parent::__construct($handler);
    }

    //Account************************

    /**
     * DESC
     *
     * @param array $params
     *
     * @return mixed
     * @throws \Exception
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function createAccount(array $params = [])
    {
        if (!count($params)) {
            throw new \Exception('Empty Params!');
        }
        $endpoint = ConfigLoader::getEndpoint(static::CREATE_ACCOUNT);

        $detail = $this->handler->request($endpoint['method'], $endpoint['path'], [
            'params' => $params,
        ]);

        return $detail;
    }

    /**
     * DESC
     *
     * @param array $params
     *
     * @return mixed
     * @throws \Exception
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function createMailbox(array $params = [])
    {
        if (!count($params)) {
            throw new \Exception('Empty Params!');
        }
        $endpoint = ConfigLoader::getEndpoint(static::CREATE_MAILBOX);

        $detail = $this->handler->request($endpoint['method'], $endpoint['path'], [
            'params' => $params,
        ]);

        return $detail;
    }

    /**
     * DESC
     *
     * @param string $contextio_token
     *
     * @return mixed
     * @throws \Exception
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function connectTokenCallbackUrl($contextio_token = '')
    {
        if (!$contextio_token) {
            throw new \Exception('contextio_token is required!');
        }

        $endpoint = ConfigLoader::getEndpoint(static::CONNECT_TOKEN_CALLBACK_URL, ['contextio_token' => $contextio_token]);

        $detail = $this->handler->request($endpoint['method'], $endpoint['path']);

        return $detail;
    }

    /**
     * DESC
     *
     * @param string $primaryAccountId
     *
     * @return mixed
     * @throws \Exception
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function getActiveMailboxes($primaryAccountId = '')
    {
        if (!$primaryAccountId) {
            throw new \Exception('primaryAccountId is required!');
        }

        $endpoint = ConfigLoader::getEndpoint(static::GET_ACTIVE_MAILBOXES, ['primaryAccountId' => $primaryAccountId]);
        $detail   = $this->handler->request($endpoint['method'], $endpoint['path']);

        return $detail;
    }

    /**
     * DESC
     *
     * @param string $primaryAccountId
     *
     * @return mixed
     * @throws \Exception
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function syncActiveMailboxes($primaryAccountId = '')
    {
        if (!$primaryAccountId) {
            throw new \Exception('primaryAccountId is required!');
        }

        $endpoint = ConfigLoader::getEndpoint(static::SYNC_ACTIVE_MAILBOXES, ['primaryAccountId' => $primaryAccountId]);

        $detail = $this->handler->request($endpoint['method'], $endpoint['path']);

        return $detail;
    }

    /**
     * DESC
     *
     * @param array  $params
     * @param string $primaryAccountId
     *
     * @return mixed
     * @throws \Exception
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function retrieveAttachments(array $params = [], $primaryAccountId = '')
    {
        if (!$primaryAccountId) {
            throw new \Exception('primaryAccountId is required!');
        }

        $endpoint = ConfigLoader::getEndpoint(static::RETRIEVE_ATTACHMENTS, ['primaryAccountId' => $primaryAccountId]);

        $detail = $this->handler->request($endpoint['method'], $endpoint['path'], [
            'params' => $params,
        ]);

        return $detail;
    }

    /**
     * DESC
     *
     * @param string $primaryAccountId
     * @param string $file_id
     *
     * @return mixed
     * @throws \Exception
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function downloadAttachment($primaryAccountId = '', $file_id = '')
    {
        if (!$primaryAccountId || !$file_id) {
            throw new \Exception('primaryAccountId and file_id are required!');
        }
        
        $endpoint = ConfigLoader::getEndpoint(static::DOWNLOAD_ATTACHMENT,
            [
                'primaryAccountId' => $primaryAccountId,
                'file_id'          => $file_id,
            ]);

        $detail = $this->handler->request($endpoint['method'], $endpoint['path']);

        return $detail;
    }

    /**
     * DESC
     *
     * @return mixed
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidEndpointException
     *
     * @author Ibraheem Abu Kaff <ibra.abukaff@tajawal.com>
     *
     */
    public function retrieveMimeTypes()
    {
        $endpoint = ConfigLoader::getEndpoint(static::RETRIEVE_MIME_TYPES);

        $detail = $this->handler->request($endpoint['method'], $endpoint['path']);

        return $detail;
    }


}
<?php

namespace ibraheem_91\AttachmentManager\Clients;

use ibraheem_91\AttachmentManager\Contracts\HandlerInterface;

/**
 * Class BaseClient
 *
 * @package ibraheem_91\AttachmentManager\Clients
 */
abstract class BaseClient
{
    /**
     * @var \ibraheem_91\AttachmentManager\Contracts\HandlerInterface
     */
    protected $handler;

    /**
     * Client constructor.
     *
     * @param \ibraheem_91\AttachmentManager\Contracts\HandlerInterface $handler
     */
    public function __construct(HandlerInterface $handler)
    {
        $this->handler = $handler;
    }
}

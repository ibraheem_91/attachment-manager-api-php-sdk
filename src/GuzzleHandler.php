<?php

namespace ibraheem_91\AttachmentManager;

use __;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;
use ibraheem_91\AttachmentManager\Contracts\HandlerInterface;
use ibraheem_91\AttachmentManager\Exceptions\HandlerException;
use ibraheem_91\AttachmentManager\Exceptions\InvalidArgumentException;

/**
 * Class GuzzleHandler
 *
 * @package ibraheem_91\AttachmentManager
 */
class GuzzleHandler implements HandlerInterface
{
    /**
     * @var \GuzzleHttp\Client
     */
    public $client;

    /**
     * @var
     */
    protected $stats;

    /**
     * The status codes that are to be taken lightly and no exception will be thrown when received
     *
     * @var array
     */
    protected $ignoredCodes = [
        200,        // All Good
        201,        // Created
        202,        // Delete request submitted
        204,        // Updated
        304,        // Not modified
        301,        // Moved permanently
        302,        // Moved temporarily
    ];

    /**
     * GuzzleHandler constructor.
     *
     * @param       $baseUrl
     * @param array $options
     *
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidArgumentException
     */
    public function __construct($baseUrl, $options = [])
    {
        $this->prepareClient($baseUrl, $options);
    }

    /**
     * Prepares the client for HTTP requests i.e. setting base URL, default options,
     * headers etc.
     *
     * @param array $baseUrl
     * @param array $options
     *
     * @return mixed
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InvalidArgumentException
     */
    public function prepareClient($baseUrl, $options = [])
    {
        $defaultHeaders = ConfigLoader::get('sdk.default_headers');
        $headers        = __::get($options, 'headers', []);

        // To populate any missing headers with the default values
        $headers = array_merge($defaultHeaders, $headers);

        if (filter_var($baseUrl, FILTER_VALIDATE_URL) === false) {
            throw new InvalidArgumentException('Invalid base url provided');
        }

        $this->client = new Client([
            'base_uri'        => $baseUrl,
            'headers'         => $headers,
            'http_errors'     => false,      // We'll handle them ourselves based upon the response code
            'allow_redirects' => [
                'max'             => 10,        // allow at most 10 redirects.
                'strict'          => true,      // use "strict" RFC compliant redirects.
                'referer'         => true,      // add a Referer header
                'protocols'       => ['https', 'http'], // only allow https URLs
                'track_redirects' => true,
            ],
            'on_stats'        => function (TransferStats $stats) {
                $this->stats = $stats;
            },
        ]);

        return $this->client;
    }

    /**
     * Sends GET request to a URL. Is a wrapper around `request` method
     *
     * @param string $endpoint
     * @param array  $options
     *
     * @return mixed
     */
    public function get($endpoint, $options = [])
    {
        return $this->request('GET', $endpoint, $options);
    }

    /**
     * To send a request of any kind to the provided URL. Al the other methods
     * use this function to send HTTP reqeusts
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $options
     *
     * @return mixed
     * @throws \ibraheem_91\AttachmentManager\Exceptions\BadRequestException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\HandlerException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InternalApiException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\InternalException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\NotFoundException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\RequestTimeoutException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\RequestUriTooLongException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\UnauthorizedException
     * @throws \ibraheem_91\AttachmentManager\Exceptions\UnprocessableEntityException
     */
    public function request($method, $endpoint, $options = [])
    {
        try {
            $requestDetail = [
                'json'    => __::get($options, 'params', ''),
                'query'   => __::get($options, 'query', ''),
                'headers' => __::get($options, 'headers', ''),
            ];

            $requestDetail = array_filter($requestDetail);

            // Send the request with required parameters
            $response = $this->client->request($method, $endpoint, $requestDetail);

            $responseCode = $response->getStatusCode();
            $response     = $response->getBody()->getContents();

            // If one of the *OK* codes, return the response
            if (in_array($responseCode, $this->ignoredCodes)) {
                return $this->transformJson($response);
            }

            // There were no guzzle exceptions but the problem
            // in request forwarded to the server
            ExceptionHandler::handleByCode($responseCode, $response);

        } catch (RequestException $e) {

            $message = '';

            if ($e->hasResponse()) {
                $message .= 'Response: ' . $e->getResponse() . ' ' . PHP_EOL;
            }

            $message .= $e->getMessage();

            throw new HandlerException($message);
        }
    }

    /**
     * Transforms from json to array
     *
     * @param $json
     *
     * @return mixed
     */
    private function transformJson($json)
    {
        return json_decode($json, true);
    }

    /**
     * Sends POST request to a URL. Is a wrapper around `request` method
     *
     * @param string $endpoint
     * @param array  $options
     *
     * @return mixed
     */
    public function post($endpoint, $options = [])
    {
        return $this->request('POST', $endpoint, $options);
    }

    /**
     * Sends a PUT request to provided URL. Wraps around the request method
     *
     * @param       $endpoint
     * @param array $options
     *
     * @return mixed
     */
    public function put($endpoint, $options = [])
    {
        return $this->request('PUT', $endpoint, $options);
    }

    /**
     * Sends a DELETE request to provided URL. Wraps around the request method
     *
     * @param string $endpoint
     * @param array  $options
     *
     * @return mixed
     */
    public function delete($endpoint, $options = [])
    {
        return $this->request('DELETE', $endpoint, $options);
    }

    /**
     * Gets the stats for the last request
     *
     * @return mixed
     */
    public function getStats()
    {
        return $this->stats;
    }
}

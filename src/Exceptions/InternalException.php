<?php
namespace ibraheem_91\AttachmentManager\Exceptions;

/**
 * Class InternalException
 *
 * @package ibraheem_91\AttachmentManager\Exceptions
 */
class InternalException extends HttpException
{
    /**
     * @var string
     */
    protected $status = '500';
    protected $title  = 'Internal Exception';
    protected $detail = '';

    /**
     * InternalException constructor.
     *
     * @param string $detail
     * @param string $title
     */
    public function __construct($detail, $title = '')
    {
        $this->detail = $detail ?: $this->detail;
        $this->title  = $title ?: $this->title;
        parent::__construct($this->detail);
    }
}

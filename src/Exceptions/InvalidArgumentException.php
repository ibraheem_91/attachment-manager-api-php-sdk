<?php

namespace ibraheem_91\AttachmentManager\Exceptions;

use Exception;

/**
 * Class InvalidArgumentException
 *
 * @package ibraheem_91\AttachmentManager\Exceptions
 */
class InvalidArgumentException extends Exception
{
}
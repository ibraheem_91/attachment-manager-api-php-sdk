<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Request Headers
    |--------------------------------------------------------------------------
    |
    | For the headers specified in the below array, if they are not provided
    | while instantiating the headers, these default values will be used.
    | For example if `User-Agent` header was not provided in the initialization
    | 'price-rule-api-sdk/1.0' will be used for it.
    */
    'default_headers' => [
        'User-Agent' => 'attachment-manager-api-php-sdk/1.0',
        'Accept'     => 'application/json',
    ],
];

